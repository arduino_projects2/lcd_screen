#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

String input = "";

void setup() {
    Serial.begin(9600);
    lcd.init();                      // initialize the lcd
    // Print a message to the LCD.
    lcd.backlight();
    lcd.setCursor(0, 0);
    lcd.print("Hello, world!");
    lcd.setCursor(0, 1);
    lcd.print("Enter Something");
}


void loop() {
    // send data only when you receive data:
    if (Serial.available() > 0) {
        lcd.clear();
        lcd.home();
        // read the incoming :
        input = Serial.readString();

        // say what you got:
        lcd.setCursor(0, 0);
        lcd.print("I received: ");
        lcd.setCursor(0, 1);
        lcd.print(input);
        Serial.println(input);
        delay(1000);

    }


}